import requests
import re
from requests.exceptions import RequestException

class LatestBook:
    def getPage(self, url):
        try:
            response = requests.get(url)
            if response.status_code == 200:
                self.__urlText = response.text
        except RequestException:
            self.__urlText = ''

    def parseUrl(self):
        pattern = re.compile('<li>.*?detail-frame.*?<h2>.*?href="(.*?)">(.*?)</a>.*?'
                             +'<span.*?gray">(.*?)</span>.*?'
                              +'<p.*?gray">(.*?)</p>.*?</li>', re.S)
        items = re.findall(pattern, self.__urlText)
        for item in items:
            url, name, score, info = item
            print(url, name, score.strip(), info.strip())

    def getUrlText(self):
        return self.__urlText

def main():
    test = LatestBook()
    test.getPage('https://book.douban.com/latest?icn=index-latestbook-all')
    print(test.getUrlText())
    test.parseUrl()


if __name__ == '__main__':
    main()