import json
import re
import requests
from requests.exceptions import RequestException
import threading

class Latest(object):
    __urlText = ''
    def __init__(self):
        self.__urlText = ''

    def getPage(self, url):
        try:
            response = requests.get(url)
            if response.status_code == 200:
                self.__urlText = response.text
        except RequestException:
            return None

    def parseUrl(self):
        pattern = re.compile('<h4.*?one-line.*?href="(.*?)".*?}">(.*?)</a>.*?</h4>', re.S)
        items = re.findall(pattern, self.__urlText)
        # for item in items:
        #     url, name = item
        #     print(url, name)
        for item in items:
            yield {
                'url': item[0],
                'title': item[1]
            }

    def getUrl(self):
        return self.__urlText

    def writeToFile(self, content):
        lock.acquire()
        with open('latestFilmInfo.txt', 'a', encoding='utf-8') as f:
            f.write(json.dumps(content, ensure_ascii=False) + '\n')
            f.close()
        lock.release()

def main(offset):
    test = Latest()
    url = 'https://maoyan.com/news?showTab=2&offset=' + str(offset)
    test.getPage(url)
    # print(test.getUrl())
    for item in test.parseUrl():
        test.writeToFile(item)

if __name__ == '__main__':
    l = []
    lock = threading.Lock()
    for i in range(1000):
        t = threading.Thread(target=main(i * 10), args=())
        t.start()
        l.append(t)
    for t in l:
        t.join()
    # for i in range(100):
    #     main(i * 10)
